import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "ACccf49e10d463482ea99d99c523c590dd";
    public static final String AUTH_TOKEN = "5efeab0d52a2ac7be6171929e6848fbd";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380505141323"), /*my phone number*/
                        new PhoneNumber("+14086375865"), str).create(); /*attached to me number*/
    }
}
